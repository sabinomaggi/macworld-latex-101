### README ###


Questo archivio contiene i file di supporto alla serie di articoli su LaTeX pubblicata su Macworld Italia.
I file sono divisi in cartelle separate in base al numero di Macworld Italia su cui è stato pubblicato l'articolo a cui si riferiscono.

---

>[![](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png "Logo Licenza Creative Commons")](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it)
>Quest'opera è distribuita secondo la licenza [Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 3.0 Unported](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it).
>Tale licenza consente a chiunque di modificare, ridistribuire, ottimizzare ed utilizzare i suddetti a fini non commerciali, dandone credito all'autore originale e licenziando i file derivati sotto i medesimi termini di licenza.
